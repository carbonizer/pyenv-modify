# pyenv-modify

## Purpose

In general, it makes sense to preserve the version name that is used by the install command (if I just ran `pyenv install 3.5.1`, I set the local version with `pyenv local 3.5.1`.  However, in the interest of compatibility with the 32-bit system with which I was working, I wanted a 32-bit version of python running on my 64-bit machine.  The problem was, since there was no way to change the "output" version name, installing a 32-bit version and then trying to install the 64-bit version (or vice versa) pyenv would complain that the indicated version was already installed.

If I remember correctly, I tried to do the most obvious thing of changing the name of the version folder after installation.  However, that broke what I assume were a bunch of links.  pyenv needs to know the intended output path while it is installing.

I then discovered that `pyenv` uses the environment variable `VERSION_NAME` to set the "output" version name.  This led to me trying to build my 32-bit Python with the command `env VERSION_NAME=2.7.11-32 pyenv install 2.7.11`.  Unfortunately, out of the box, `pyenv` `unset`s `VERSION_NAME`.

This script will locate `python-install` in `PYENV_ROOT` and comment out `unset VERSION_NAME` such that the previously mentioned command will result in a pyenv version named `2.7.11-32`.

## Usuage

By default, the tool runs interactively, requiring the user to indicate that the correct `python-install` has been located correctly.  However, the `-I` flag can be used to run non-interactively.  Regardless of how the script is run, a backup of `pyenv-install` is created that can be later restored manually, if necesssary.